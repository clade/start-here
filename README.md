# Start-Here

To get started with the clade project, this project will be where al the documentation is required to setup and run a local instance of Clade.

## Setting up the local docker image

Install docker for your local enviornment, see [here](https://www.docker.com/get-started)

1. docker pull serialbandicoot/wave-hub/latest
2. docker run -i -t -p 127.0.0.1:3000:3000 serialbandicoot/wave-hub:latest
3. cd /var/www
4. git clone https://gitlab.com/clade/clade-data-feeds.git
5. git clone https://gitlab.com/clade/component-api.git
6. cd into clade-data-feeds
7. pip3 install -r requirements.txt
8. cd into component-api
9. pip3 install -r requirements.txt

### Testing Mongo Database

1. mongo 
2. If you get the error: Failed to connect to 127.0.0.1:27017 try: `rm /var/lib/mongodb/mongod.lock`
3. try mongo again

### Clade Reference Data

1. cd clade-data-feeds/mongo_config
2. python3 load_it.py --data company_data.json
3. python3 load_it.py --data schedule.json
4. Check in mongo
5. mongo
6. use clade
7. db.company_data.find() to check, switch to schedule collection to check also

### Schedule

1. The schedule works on 15-20 min timers, update the schedule.json and re-run load_it.py 

### Data feeds

1. python3 news.py / company_fundamentals.py / dividend_fundamentals.py / dividend_fundamentals2.py
2. On the devlopment enviornment, you will only 10 or companies of each. 
3. Set the period in the schedule.json and re-load to increases the datafeed polling times. 

### Start up component service

1. Navigate to component-api
2. export LC_ALL=C.UTF-8
3. export LANG=C.UTF-8
4. pip3 install python-dotenv
5. flask run
6. flask should be running on port 0.0.0.0:3000
7. Outside of docker, local enviornment, open a browser and check http://localhost:3000/news?symbol=AAPL

